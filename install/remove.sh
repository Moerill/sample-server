#!/usr/bin/env bash
set -e

rm /etc/cron.daily/foundry-save.sh
systemctl disable foundry
service foundry stop
rm /etc/systemd/system/foundry.service
rm -R /opt/foundry-server
deluser foundry







