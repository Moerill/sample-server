#!/usr/bin/env bash

server_repo=git@gitlab.com:Moerill/sample-server.git

if [ -z "$1" ]; then
    >&2 echo "You have to supply the FoundryVTT download url as an argument to this script."
    exit 1
fi
foundry_download_url=$1

useradd -r -s /bin/false foundry
usermod -L foundry

#mkdir -p /opt/foundry-server
#cd /opt/foundry-server || exit 1
cd ~/valiCrypt/foundry/foundry

#git clone $server_repo .
#exit 1
git submodule update --init --recursive
npm ci
npm run install-foundry -- $foundry_download_url
npm run update-data
chown -R foundry:foundry .

cp install/service/* /etc/systemd/system
systemctl daemon-reload
service foundry start
systemctl enable foundry

cp install/cron/foundry-save.sh /etc/cron.daily
chmod +x /etc/cron.daily/foundry-save.sh
