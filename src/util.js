import {join} from 'path';
import fs from 'fs-extra';
import {spawn} from 'child_process';
import readline from 'readline';
import {createReadStream} from 'fs';


export function readdirSyncRecursiveImpl(dir, curdir = dir, fileList = []) {
	return fs.readdirSync(curdir, {withFileTypes: true}).flatMap(file => {
		const curFilePath = join(curdir, file.name);
		return file.isDirectory()
			? readdirSyncRecursiveImpl(dir, curFilePath, fileList)
			: fileList.concat(relative(dir, curFilePath));
	});
}

async function spawnWithDefaults(cmd, args, options = {}) {
	options.env = options.env || {};
	options.env = {
		ADBLOCK: `true`,
		...process.env,
		...options.env
	};
	let finalOptions = {
		encoding: `utf8`,
		shell: true,
		...options
	};
	const execution = spawn(cmd, args, finalOptions);

	return new Promise((resolve, reject) => {
		let stdout = ``;

		execution.stdout.on(`data`, (data) => {
			if (!finalOptions.quiet) {
				process.stdout.write(data);
			}
			stdout += data.toString(`utf8`);
		});

		let stderr = ``;
		execution.stderr.on(`data`, (data) => {
			if (!finalOptions.quiet) {
				process.stdout.write(data);
			}
			stderr += data.toString(`utf8`);
		});

		execution.on('error', (error) => {
			reject({
				error
			});
		});

		execution.on('close', (code) => {
			const result = {
				stdout,
				stderr,
				code
			};
			(code === 0 ? resolve : reject)(result);
		});
	});
}

export async function runCommand(cmd, options = {}) {
	const name = options.name ? `${options.name}: ` : '';
	if (!options.quiet) {
		console.log(`Running command ${name}"${cmd}"`);
	}
	const [actualCmd, ...args] = cmd.split(` `);
	const out = await spawnWithDefaults(actualCmd, args, options);
	if (!options.quiet) {
		console.log(`Finished running command ${name}"${cmd}"`);
	}
	return out;
}

export function mockGlobalExpressEmit() {
	global.express = {io: {emit: () => {}}};
}

export async function readLines(path) {
	const reader = readline.createInterface({
		input: createReadStream(path)
	});

	const lines = [];
	for await (const line of reader) {
		lines.push(line);
	}
	return lines;
}

export async function writeLines(path, lines) {
	return fs.writeFile(path, lines.join(`\n`));
}

function logWithArgs(msg, args) {
	const msgWithArgs = args.length ? `${msg} with arguments` : msg;
	console.log(msgWithArgs, ...args);
}

export function logExecution(fn, name = fn.name || `anonymous function`) {
	return async (...args) => {
		logWithArgs(`Starting ${name}`, args);
		const result = await fn(...args);
		logWithArgs(`Finished ${name}`, args);
		return result;
	};
}
