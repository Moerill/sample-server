'use strict';


import {logExecution} from './util.js';
import installWorlds from './install-worlds.js';
import installModules from './install-modules.js';

logExecution(async () => {
	await installWorlds();
	await installModules();
}, `updateData`)();
