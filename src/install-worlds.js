'use strict';

import fs from 'fs-extra';
import path from 'path';
import {appWorldsDir, worldsDir} from './dirs.js';
import {logExecution} from './util.js';


const lockFile = path.join(appWorldsDir, `.worldsInstalled.lock`);

function worldsDirHasContent(dir) {
	const fileList = fs.readdirSync(dir);
	return fileList.length !== 1 || fileList[0] !== `README.txt`;
}

export default logExecution(async () => {
	if (fs.existsSync(lockFile)) {
		console.log(`Worlds are already installed.`);
		return Promise.resolve();
	}

	if (worldsDirHasContent(appWorldsDir)) {
		throw `The app worlds dir (${appWorldsDir}) already has content, but has not been installed from the repo!`;
	}

	fs.copySync(worldsDir, appWorldsDir);
}, `installWorlds`);


