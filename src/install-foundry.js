'use strict';
import patchMainJs from './patch-main.js';
import path from 'path';
import fs from 'fs-extra';
import download from 'download';

import {appDir, downloadDir} from './dirs.js';
import {logExecution} from './util.js';

const zipUrl = process.argv[2];

const downloadFoundry = logExecution(async () => {
	await download(zipUrl, downloadDir, {
		extract: true,
		filter: file => {
			return file.path.startsWith('resources/app/');
		}
	});
}, `downloadFoundry`);

logExecution(async () => {
	await fs.removeSync(downloadDir);

	if (fs.existsSync(appDir)) {
		throw `Can't install, app/ already exists. Use update.`;
	}

	await downloadFoundry();

	var unzippedAppDir = path.join(downloadDir, `resources`, `app`);
	await fs.moveSync(unzippedAppDir, appDir);

	await fs.removeSync(downloadDir);

	await patchMainJs();
}, `installFoundry`)();
