'use strict';

const updateKey = process.argv[2];

import {runCommand, logExecution} from './util.js';
import patchMainJs from './patch-main.js';

logExecution(async () => {
	const updateFoundryCmd = `node --unhandled-rejections=strict --experimental-modules src/update-foundry-subprocess.js ${updateKey}`;
	await logExecution(async () => await runCommand(updateFoundryCmd), `updateApp`)();
	await patchMainJs();
}, `updateFoundry`)();
