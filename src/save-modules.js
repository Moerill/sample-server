'use strict';

import fs from 'fs-extra';
import path from 'path';
import saveSubmodule from './save-submodule.js';
import {logExecution} from './util.js';
import {appModulesDir, modulesDir} from './dirs.js';

export default logExecution(async () => {
	saveSubmodule(`modules`, logExecution(async () => {
		const files = fs.readdirSync(appModulesDir, {withFileTypes: true});
		const moduleJsonUrls = files
			.filter(f => f.isDirectory())
			.map(dir => path.join(appModulesDir, dir.name, `module.json`))
			.map(manifestFile => fs.readFileSync(manifestFile))
			.map(manifestStr => JSON.parse(manifestStr))
			.map(manifest => {
				if (!manifest.manifest) {
					console.log(`Module ${manifest.name} does not contain URL to its manifest, can not save!`);
					return null;
				}
				return manifest.manifest;
			})
			.filter(manifestUrl => manifestUrl !== null)
			.sort((str1, str2) => str1.toLowerCase().localeCompare(str2.toLowerCase()));
		const fileContent = JSON.stringify(moduleJsonUrls, null, `\t`);
		fs.writeFileSync(path.join(modulesDir, `modules.json`), fileContent);
	}, `creating module.json`));
}, `saveModules`);
