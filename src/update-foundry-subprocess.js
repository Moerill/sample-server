const updateKey = process.argv[2];

'use strict';
import fs from 'fs-extra';
import path from 'path';
import {appDir} from './dirs.js';
import {mockGlobalExpressEmit} from './util.js';
import {createRequire} from 'module';

const require = createRequire(import.meta.url);

mockGlobalExpressEmit();
global.rootDir = appDir;
global.ver = JSON.parse(fs.readFileSync(path.join(appDir, `package.json`), `utf8`)).version;
global.vtt = `Foundry Updater`;
global.options = {};

const {Updater} = require(path.join(appDir, `dist`, `update`));

(async () => {
	global.logger = require(path.join(appDir, `dist`, `logging`)).logger;

	let updater = new Updater(updateKey, appDir);

	if (!await updater.check()) return false;
	const updateResult = await updater.update(`download`);
	if (!updateResult) {
		throw `Updating failed`;
	}
})();

