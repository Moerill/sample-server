'use strict';

import saveWorlds from './save-worlds.js';
import saveModules from './save-modules.js';
import {logExecution} from './util.js';

logExecution(async () => {
	await saveWorlds();
	await saveModules();
}, `save`)();
