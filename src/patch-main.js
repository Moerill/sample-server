'use strict';

import {appDir} from './dirs.js';
import {readLines, writeLines, logExecution} from './util.js';
import path from 'path';

function applySslPatch(lines) {
	const hasCorrectSslLines = lines[33] === `  const sslKey = options.sslKey ? path.join(rootDir, options.sslKey) : null;`
		&& lines[34] === `  const sslCert  = options.sslCert ? path.join(rootDir, options.sslCert) : null;`;
	if (hasCorrectSslLines) {
		lines.splice(33, 2,
			`let sslKey = args.find(a => a.startsWith('--sslKey'));`,
			`sslKey = sslKey ? sslKey.split('=').pop() : options.sslKey;`,
			`let sslCert = args.find(a => a.startsWith('--sslCert'));`,
			`sslCert = sslCert ? sslCert.split('=').pop() : options.sslCert;`)
	} else {
		throw `SSL patch failed`;
	}
}

function applyLockfilePatch(lines) {
	const hasCorrectLockfileLines = lines[106] === `  const isLocked = await lockfile.check(optionsFile);` &&
		lines[107] ===
		'  if ( isLocked ) global.fatalError = Error(`Foundry VTT cannot start in this directory which is already locked by another process.`);' &&
		lines[108] === `  else lockfile.lock(optionsFile);`;
	if (hasCorrectLockfileLines) {
		lines.splice(106, 3);
	} else {
		throw `Lockfile patch failed`;
	}
}

export default logExecution(async () => {
	const mainJsPath = path.join(appDir, `main.js`);
	const lines = await readLines(mainJsPath);

	applySslPatch(lines);
	applyLockfilePatch(lines);

	await writeLines(mainJsPath, lines);
}, `patchMainJs`);
