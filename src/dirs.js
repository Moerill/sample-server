'use strict';

import {dirname, join} from 'path';
import {fileURLToPath} from 'url';

export const baseDir = join(dirname(fileURLToPath(import.meta.url)), `..`);
export const downloadDir = join(baseDir, `_download`);
export const worldsDir = join(baseDir, `worlds`, `data`);
export const modulesDir = join(baseDir, `modules`);
export const appDir = join(baseDir, `app`);
export const appResourcesDir = join(appDir, `public`);
export const appModulesDir = join(appResourcesDir, `modules`);
export const appWorldsDir = join(appResourcesDir, `worlds`);
