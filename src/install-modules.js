'use strict';

import {createRequire} from 'module';
import fs from 'fs-extra';
import path from 'path';
import {appDir, appResourcesDir} from './dirs.js';
import {mockGlobalExpressEmit, logExecution} from './util.js';
import {modulesDir} from './dirs.js';

const require = createRequire(import.meta.url);

global.publicDir = appResourcesDir;
mockGlobalExpressEmit();

const {Module} = require(path.join(appDir, `dist`, `packages`, `module`));

const modulesToInstall = JSON.parse(fs.readFileSync(path.join(modulesDir, `modules.json`)));

export default logExecution(async () => {
	const moduleInstallPromises = modulesToInstall
		.map((manifest) => Module.getManifest(manifest))
		.map(async (manifestPromise) => {
			const manifest = await manifestPromise;
			global.logger = {info: () => {}};
			return await Module.install(manifest.name, manifest.download);
		});

	(await Promise.all(moduleInstallPromises))
		.forEach((moduleInstalledSuccessfully, idx) => {
			if (moduleInstalledSuccessfully) {
				console.log(`${modulesToInstall[idx]} installed`)
			} else {
				console.error(`${modulesToInstall[idx]} not installed correctly`);
			}
		});
}, `installModules`);
