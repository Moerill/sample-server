'use strict';

import {runCommand, logExecution} from './util.js';

async function git(name, args, options = {}) {
	return await runCommand(`git --git-dir=${name}/.git ${args}`, {
		quiet: true,
		...options
	});
}

async function submoduleHasRemoteUpdate(name) {
	const headHash = await git(name, `rev-parse HEAD`).stdout;
	const masterHash = await git(name, `rev-parse origin/master`).stdout;
	return headHash !== masterHash;
}

export async function submoduleHasChanges(name) {
	let output = (await git(name, `status --porcelain`)).stdout;
	return output !== ``;
}


async function publishChanges(name) {
	if (!await submoduleHasChanges(name)) {
		console.log(`Submodule ${name} did not change, so can not publish any changes either.`);
		return;
	}
	console.log(`Committing changes to ${name} repo`);
	await git(name, `checkout master`);
	await git(name, `add -A`);
	const explanation = `This is an automatic update bringing the newest changes from the game to the repository`;
	await git(name, `commit -m "Regular update" -m "${explanation}"`);
	await logExecution(() => git(name, `push`, {quiet: false}), `pushing changes to ${name} repo`)();
}

export default async (name, syncFunc) => {
	if (await submoduleHasChanges(name)) {
		throw `Local ${name} repo already has changes. Please make sure no changes are being lost, and restore a changeless state.`
	}

	if (await submoduleHasRemoteUpdate(name)) {
		throw `Local ${name} repo is behind remote! This shouldn't happen, as we try to update` +
		` remote, not the other way around. Fix this manually.`;
	}

	await syncFunc();
	await publishChanges(name);
};
