'use strict';

import fs from 'fs-extra';
import saveSubmodule from './save-submodule.js';
import {appWorldsDir, worldsDir} from './dirs.js';
import {logExecution} from './util.js';

export default logExecution(async () => {
	await saveSubmodule('worlds', logExecution(async () => {
		fs.removeSync(worldsDir);
		fs.copySync(appWorldsDir, worldsDir);
	}, `copying worlds to submodule`));
}, `saveWorlds`);
