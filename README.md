This repository shows you how to set up a foundry server on a ubuntu machine so that:

1. You can automatically install/update everything whenever you want, with simple commands
2. Foundry runs as a service and on boot
3. You have daily backups of your `worlds` directory and your installed modules in git repositories through a commit history

### Request from me to you

Please report any problems you have reading this README or just simple hard to understand parts. I want to improve the documentation so it's as easy as possible to set everything up.

## Requirements

* An Ubuntu 16.04+ machine (Debian maybe works as well, idk)
* Git installed
* Node (probably v10+) installed
* At least basic knowledge about how the above technologies work, this is not made for a complete layman

## How it works

A few node.js scripts have been written to provide the above features. They can be called with `npm run <script> -- <args>` and are listed here:

| Script | Arguments | Description |
| ------ | ---- | ----------- |
| start  | Any that foundry itself takes | Starts the foundry server. |
| install-foundry | The FoundryVTT download link | Installs FoundryVTT in the `app/` directory |
| update-foundry | The FoundryVTT update key | Updates FoundryVTT as if you had used the "Update Software" function in the `/setup` endpoint |
| save-data | None | Saves world data & installed modules to the git repositories (added as submodules to this repository) in the `worlds/` and `modules/` directories |
| update-data | None | Adds the saved world data & installed modules to the FoundryVTT `public/` directory |

There's a system.d service file that basically runs `npm start`.

There's a cron job that runs `npm run save-data` daily.

There's a setup script that:
 
* Checks out this repository
* Runs the `install-foundry` and `update-data` scripts
* Creates a user (`foundry`) the server will run on
* Installs the system.d service and sets it to run on boot
* Adds the save-data cron job to `/etc/cron.daily`

## How to use this for yourself

### Install

1. Fork this repository and the submodule repositories [worlds](https://gitlab.com/foundry-azzurite/sample-worlds/) and [modules](https://gitlab.com/foundry-azzurite/sample-modules)
   * The worlds & modules repositories are your backup storage. A commit will be created every day and you can restore world state from these commits if you know how to use git.
1. Edit the `install/install.sh` file and change the variable `server_repo` at the top to point to your fork of this repository
1. Edit the `.gitmodules` file and update the `url`s of the submodules to your just created forks of these repositories
1. Run `git submodule sync`
1. Commit & push these `install.sh` and submodule changes to your fork
1. Copy `install.sh` to your server and run it as root and with a FoundryVTT download link as argument (`sudo ./install.sh http://URL`). Everything will be installed into the `/opt/foundry-server/` directory.
1. (Optional) Copy your `public/modules/` and `public/worlds/` directories from your existing FoundryVTT installation to `/opt/foundry-server/app/public/` on your server and run `npm run save-data`
1. (Optional) Adjust the line `ExecStart=/usr/bin/npm start` in `/etc/systemd/system/foundry.service` to your liking.
   
   For example, you could add additional parameters like `--sslKey`, `--sslCert`, `--world` or `--port`. I use something like this:
   
   ```
   ExecStart=/usr/bin/npm start -- --sslKey=/etc/letsencrypt/live/[redacted]/privkey.pem --sslCert=/etc/letsencrypt/live/[redacted]/fullchain.pem --world=[redacted]
   ```
   
### Usage

After installation, your FoundryVTT server should be running. Use `service foundry status` to show its status. It's a normal system.d service so everything related should work for it.

If a new version of foundry comes out, you want to use `npm run update-foundry -- updatekey` instead of using the web interface or doing it manually.

If you want to save your worlds/modules manually, run `npm run save-data`.

If you want to restore any previous version of your worlds/modules, simply checkout the desired version in the respective submodules and run `npm run update-data`. If you want to restore a previous world state, you'll have to manually delete `app/public/worlds/.worldsInstalled.lock`, otherwise it'll not overwrite your worlds.

If I update this repo in the future and you want the changes, you are expected to know what you're doing and how to incorporate the changes. 
